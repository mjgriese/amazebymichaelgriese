/**Homepage for the maze application. From this page, the generation begins based on the interface options selected by the user.
 * @author Michael Griese
 * @date November 18, 2019
 */
package edu.wm.cs.cs301.amazebymichaelgriese.gui;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.ValueAnimator;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.ArrayAdapter;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Button;
import android.content.Intent;
import android.widget.TextView;
import android.widget.Toast;

import edu.wm.cs.cs301.amazebymichaelgriese.R;

public class AMazeActivity extends AppCompatActivity {

    private Spinner builderSpinner;
    private Spinner solverSpinner;
    private Button revisitButton;
    private Button exploreButton;
    private SeekBar difficultyMeter;
    private SeekBar energyMeter;
    private Spinner roomSpinner;

    /**This is the main method that identifies and references all the spinners and buttons that the user
     * is allowed to interact with.
     * @param savedInstanceState    required parameter, honestly no clue what it does.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_maze_activity_layout);

        //creates a reference to the builder spinner to populate and get values from
        builderSpinner = findViewById(R.id.builderSpinner);
        ArrayAdapter<CharSequence> builderAdapter = ArrayAdapter.createFromResource(this, R.array.builder, android.R.layout.simple_spinner_item);
        builderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        builderSpinner.setAdapter(builderAdapter);

        //creates a reference to the driver spinner to populate and get values from
        solverSpinner = findViewById(R.id.solverSpinner);
        ArrayAdapter<CharSequence> solverAdapter = ArrayAdapter.createFromResource(this, R.array.driver, android.R.layout.simple_spinner_item);
        solverAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        solverSpinner.setAdapter(solverAdapter);

        //create the room spinner
        roomSpinner = findViewById(R.id.roomSpinner);
        ArrayAdapter<CharSequence> roomAdapter = ArrayAdapter.createFromResource(this, R.array.roomPrompt, android.R.layout.simple_spinner_item);
        roomAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        roomSpinner.setAdapter(roomAdapter);

        //add references to each button
        exploreButton = findViewById(R.id.exploreButton);
        revisitButton = findViewById(R.id.revisitButton);
        //add click listener to allow the buttons to do things when clicked
        exploreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //log("explore button clicked");
                launchExplore();
            }
        });

        revisitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //log("revisit button clicked");
                launchRevisit();
            }
        });

        difficultyMeter = findViewById(R.id.difficultyMeter);
        energyMeter = findViewById(R.id.energyBar);

        final TextView animateTextView = (TextView)findViewById(R.id.animation);
        // construct the value animator and define the range

        ValueAnimator valueAnimator = ValueAnimator.ofFloat(0f, 360f);
        //repeats the animation 2 times
        valueAnimator.setRepeatCount(ValueAnimator.INFINITE);
        valueAnimator.setInterpolator(new LinearInterpolator()); // increase the speed first and then decrease
        // animate over the course of 700 milliseconds
        valueAnimator.setDuration(1500);
        // define how to update the view at each "step" of the animation
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float progress = (float) animation.getAnimatedValue();
                animateTextView.setRotationX(progress);

            }
        });
        valueAnimator.start();
    }

    /**
     * This is  the method that the explore button triggers when pressed.
     * The next screen is created (intent) and the user selected items are placed into it as extra
     * strings.
     * "GENERATE" either holds the generation alg or it is false
     * "SOLVER" holds the solving method to be used (manual / wizard)
     * "BUTTON" holds whichever button was selected (important for generation vs. loading a file)
     */
    //method to start the maze generation from scratch
    public void launchExplore() {
        Intent stateGenerating = new Intent(this, GeneratingActivity.class);
        stateGenerating.putExtra("GENERATE", String.valueOf(builderSpinner.getSelectedItem()));
        stateGenerating.putExtra("SOLVER", String.valueOf(solverSpinner.getSelectedItem()));
        stateGenerating.putExtra("ROOMS", String.valueOf(roomSpinner.getSelectedItem()));
        stateGenerating.putExtra("BUTTON", "explore");
        stateGenerating.putExtra("DIFF", String.valueOf(difficultyMeter.getProgress()));
        stateGenerating.putExtra("ENERGY", String.valueOf(energyMeter.getProgress()));
        startActivity(stateGenerating);
    }

    /**
     * This is  the method that the revisit button triggers when pressed.
     * The next screen is created (intent) and the user selected items are placed into it as extra
     * strings.
     * "GENERATE" either holds the generation alg or it is false
     * "SOLVER" holds the solving method to be used (manual / wizard)
     * "BUTTON" holds whichever button was selected (important for generation vs. loading a file)
     */
    //method that will read in the maze from a file
    public void launchRevisit(){
        Intent stateGenerating = new Intent(this, GeneratingActivity.class);

        stateGenerating.putExtra("GENERATE", "false");
        stateGenerating.putExtra("SOLVER", String.valueOf(solverSpinner.getSelectedItem()));
        stateGenerating.putExtra("BUTTON", "revisit");
        stateGenerating.putExtra("ROOMS", String.valueOf(roomSpinner.getSelectedItem()));
        stateGenerating.putExtra("DIFF", String.valueOf(difficultyMeter.getProgress()));
        stateGenerating.putExtra("ENERGY", String.valueOf(energyMeter.getProgress()));
        startActivity(stateGenerating);
    }

    /**
     * This method overrides the default behavior of the back button. It forces the user to remain
     * in the application even from the home menu.
     */
    @Override
    public void onBackPressed() {

    }

    /**
     * This is a method that both shows a toast of the passed string as well as logs it using Log.v
     * @param string is the string that will be toasted as well as the value of the log
     * The tag for every log is the name of the activity it came from (AMazeActivity)
     */
    private void log(String string) {
        Toast.makeText(getApplicationContext(), string, Toast.LENGTH_SHORT).show();
        Log.v("AMazeActivity", string);
    }
}
