package edu.wm.cs.cs301.amazebymichaelgriese.gui;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.drawable.Drawable;


import java.util.Random;

import edu.wm.cs.cs301.amazebymichaelgriese.R;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

class SnowFlake {
    private static final float ANGE_RANGE = 0.1f;
    private static final float HALF_ANGLE_RANGE = ANGE_RANGE / 2f;
    private static final float HALF_PI = (float) Math.PI / 2f;
    private static final float ANGLE_SEED = 25f;
    private static final float ANGLE_DIVISOR = 10000f;
    private static final float INCREMENT_LOWER = 2f;
    private static final float INCREMENT_UPPER = 4f;
    private static final float FLAKE_SIZE_LOWER = 70f;
    private static final float FLAKE_SIZE_UPPER = 140f;
    private static final int NUMBER_OF_UFO = 2;

    private final Random random;
    private final Point position;
    private float angle;
    private final float increment;
    private final float flakeSize;
    private final Paint paint;

    public static SnowFlake create(int width, int height, Paint paint) {
        Random random = new Random();
        int x = random.nextInt(width);
        int y = random.nextInt(height);
        Point position = new Point(x, y);
        float angle = random.nextInt((int)((ANGLE_SEED) / ANGLE_SEED * ANGE_RANGE + HALF_PI - HALF_ANGLE_RANGE));
        float increment = random.nextInt((int)((INCREMENT_UPPER - INCREMENT_LOWER) + INCREMENT_LOWER));
        float flakeSize = random.nextInt((int)((FLAKE_SIZE_UPPER - FLAKE_SIZE_LOWER) + FLAKE_SIZE_LOWER));
        return new SnowFlake(random, position, angle, increment, flakeSize, paint);
    }

    SnowFlake(Random random, Point position, float angle, float increment, float flakeSize, Paint paint) {
        this.random = random;
        this.position = position;
        this.angle = angle;
        this.increment = increment;
        this.flakeSize = flakeSize;
        this.paint = paint;
    }

    private void move(int width, int height) {
        double x = position.x + (increment * cos(angle));
        double y = position.y + (increment * sin(angle));

        angle += random.nextInt((int)(ANGLE_SEED + ANGLE_SEED)) / ANGLE_DIVISOR;

        position.set((int) x, (int) y);

        if (!isInside(width, height)) {
            reset(width);
        }
    }

    private boolean isInside(int width, int height) {
        int x = position.x;
        int y = position.y;
        return x >= -flakeSize - 1 && x + flakeSize <= width && y >= -flakeSize - 1 && y - flakeSize < height;
    }

    private void reset(int width) {
        position.x = random.nextInt(width);
        position.y = (int) (-flakeSize - 1);
        angle = random.nextInt((int)ANGLE_SEED) / ANGLE_SEED * ANGE_RANGE + HALF_PI - HALF_ANGLE_RANGE;
    }


    public void draw(Canvas canvas) {
        int width = canvas.getWidth();
        int height = canvas.getHeight();
        move(width, height);
        paint.setColor(Color.YELLOW);
        int[] xStarPoints = new int[5];
        int[] yStarPoints = new int[5];
        xStarPoints[0] = (int)(position.x + (flakeSize *cos(225)));
        xStarPoints[1] = (int)(position.x + (flakeSize *cos(90)));
        xStarPoints[2] = (int)(position.x + (flakeSize *cos(270)));
        xStarPoints[3] = (int)(position.x + (flakeSize *cos(135)));
        xStarPoints[4] = (int)(position.x + (flakeSize *cos(45)));

        yStarPoints[0] = (int)(position.y +(flakeSize * sin(225)));
        yStarPoints[1] = (int)(position.y +(flakeSize *sin(90)));
        yStarPoints[2] = (int)(position.y +(flakeSize * sin(270)));
        yStarPoints[3] = (int)(position.y +(flakeSize * sin(135)));
        yStarPoints[4] = (int)(position.y +(flakeSize * sin(45)));

        Path starPath = new Path();
        starPath.moveTo(xStarPoints[0], yStarPoints[0]);
        for(int point = 1; point < 5; point++){
            starPath.lineTo(xStarPoints[point], yStarPoints[point]);
        }
        starPath.close();
        canvas.drawPath(starPath, paint);
    }
}
