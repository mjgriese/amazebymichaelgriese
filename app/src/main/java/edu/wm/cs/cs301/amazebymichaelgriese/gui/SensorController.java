package edu.wm.cs.cs301.amazebymichaelgriese.gui;

import edu.wm.cs.cs301.amazebymichaelgriese.gui.Robot.Direction;
/**@author Michael Griese
 * This class is meant to disrupt the robot during runtime. The robot will have sensors disabled at a regualar interval. 
 */
public class SensorController implements Runnable {

	private Robot robot;
	private boolean[] stableList = {true, true, true, true}; //left right forward backward
	private boolean isDamaged = false;
	private Direction direction;
	private boolean isActive = false;

	public SensorController(){
	}
	
	public void setRobot(Robot robot){
		this.robot = robot;
	}
	
	public boolean getDamage(){
		return this.isDamaged;
	}

	public void setDirection(Direction direction){
		this.direction = direction;
	}

	public void toggleInterrupt(){
		isActive = !isActive;
	}

	@Override
	public void run() {
		while(true){
			while(isActive) {
				if (this.robot == null) {
					continue; //if there is no robot reference yet then do nothing
				}
				robot.triggerSensorFailure(direction);
				try {
					isDamaged = true;
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					//do nothing this shouldn't get called
				}
				robot.repairFailedSensor(Direction.BACKWARD);
				try {
					isDamaged = false;
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					//do nothing this shouldn't get called
				}
			}
		}
	}
}
