package edu.wm.cs.cs301.amazebymichaelgriese.gui;

import edu.wm.cs.cs301.amazebymichaelgriese.generation.CardinalDirection;
import edu.wm.cs.cs301.amazebymichaelgriese.generation.Distance;
import edu.wm.cs.cs301.amazebymichaelgriese.generation.Maze;
import edu.wm.cs.cs301.amazebymichaelgriese.generation.staticMazeHolder;
import edu.wm.cs.cs301.amazebymichaelgriese.gui.Robot.Direction;
import edu.wm.cs.cs301.amazebymichaelgriese.gui.Robot.Turn;

public class Wizard implements RobotDriver{

	private BasicRobot robot; //private robot reference
	private Distance dist; 
	private MazePanel mazePanel;
	private float startingBattery;
	private int width, height;
	private boolean canDrive = false;
	
	boolean[] currentSensors = {true, true, true, true}; //LEFT RIGHT FORWARD BACKWARD

	public Wizard(){}
	
	@Override
	public void setRobot(Robot r) {
		this.robot = (BasicRobot) r;		
	}

	@Override
	public void setDimensions(int width, int height) {
		this.width = width; 
		this.height = height;
		
	}

	@Override
	public void setDistance(Distance distance) {
		dist = robot.maze.getMazedists();
		
	}

	@Override
	public void triggerUpdateSensorInformation() {
		for(Direction d : Direction.values()){
			currentSensors[d.ordinal()] = robot.hasOperationalSensor(d);
		}
		
	}

	public void toggleDrive(){
		canDrive = !canDrive;
	}

	@Override
	public boolean drive2Exit() throws Exception {
		Maze maze = staticMazeHolder.getContainedMaze();
		startingBattery = this.robot.getBatteryLevel();
		while ((this.robot.hasStopped() == false) && (this.robot.isAtExit() == false)) {    //while the robot is not at the exit, go there
			while(canDrive){
				boolean nextJump = false;
				robot.updateParentEnergy();
				System.out.println("Solving...");
				mazePanel.invalidate();
				Thread.sleep(200);
				int[] cords = this.robot.getCurrentPosition();
				int[] nextMove = maze.getNeighborCloserToExit(cords[0], cords[1]);
				//add dist comparison here with wall shits
				try {
					if (dist.getDistanceValue(cords[0] - 1, cords[1]) < dist.getDistanceValue(nextMove[0], nextMove[1]) - 6) {
						nextMove[0] = cords[0] - 1;
						nextMove[1] = cords[1];
						nextJump = true;
					} else if (dist.getDistanceValue(cords[0] + 1, cords[1]) < dist.getDistanceValue(nextMove[0], nextMove[1]) - 6) {
						nextMove[0] = cords[0] + 1;
						nextMove[1] = cords[1];
						nextJump = true;
					} else if (dist.getDistanceValue(cords[0], cords[1] - 1) < dist.getDistanceValue(nextMove[0], nextMove[1]) - 6) {
						nextMove[0] = cords[0];
						nextMove[1] = cords[1] - 1;
						nextJump = true;
					} else if (dist.getDistanceValue(cords[0], cords[1] + 1) < dist.getDistanceValue(nextMove[0], nextMove[1]) - 6) {
						nextMove[0] = cords[0];
						nextMove[1] = cords[1] + 1;
						nextJump = true;
					}
				} catch (Exception e) {

				}
				//end comparisons
				CardinalDirection currentDirection = this.robot.getCurrentDirection();
				CardinalDirection desiredDirection;
				try {
					desiredDirection = CardinalDirection.getDirection(nextMove[0] - cords[0], nextMove[1] - cords[1]);
				}
				catch (NullPointerException e){
					break;
				}
				if (currentDirection == desiredDirection) {
					if (nextJump) {
						this.robot.jump();
						mazePanel.invalidate();
						Thread.sleep(200);
					} else {
						this.robot.move(1, false);
						mazePanel.invalidate();
						Thread.sleep(200);
					}
				} else {
					switch (currentDirection) {
						case East:
							switch (desiredDirection) {
								case North:
									this.robot.rotate(Turn.RIGHT);
									mazePanel.invalidate();
									Thread.sleep(200);
									break;
								case South:
									this.robot.rotate(Turn.LEFT);
									mazePanel.invalidate();
									Thread.sleep(200);
									break;
								case West:
									this.robot.rotate(Turn.AROUND);
									mazePanel.invalidate();
									Thread.sleep(200);
									break;
								default:
									break;
							}
							break;
						case North:
							switch (desiredDirection) {
								case East:
									this.robot.rotate(Turn.LEFT);
									mazePanel.invalidate();
									Thread.sleep(200);
									break;
								case South:
									this.robot.rotate(Turn.AROUND);
									mazePanel.invalidate();
									Thread.sleep(200);
									break;
								case West:
									this.robot.rotate(Turn.RIGHT);
									mazePanel.invalidate();
									Thread.sleep(200);
									break;
								default:
									break;
							}
							break;
						case South:
							switch (desiredDirection) {
								case East:
									this.robot.rotate(Turn.RIGHT);
									mazePanel.invalidate();
									Thread.sleep(200);
									break;
								case North:
									this.robot.rotate(Turn.AROUND);
									mazePanel.invalidate();
									Thread.sleep(200);
									break;
								case West:
									this.robot.rotate(Turn.LEFT);
									mazePanel.invalidate();
									Thread.sleep(200);
									break;
								default:
									break;
							}
							break;
						case West:
							switch (desiredDirection) {
								case East:
									this.robot.rotate(Turn.AROUND);
									mazePanel.invalidate();
									Thread.sleep(200);
									break;
								case South:
									this.robot.rotate(Turn.RIGHT);
									mazePanel.invalidate();
									Thread.sleep(200);
									break;
								case North:
									this.robot.rotate(Turn.LEFT);
									mazePanel.invalidate();
									Thread.sleep(200);
									break;
								default:
									break;
							}
							break;
						default:
							break;
					}//large switch breaks
					if (nextJump) {
						this.robot.jump();
						mazePanel.invalidate();
						Thread.sleep(200);
					} else {
						this.robot.move(1, false);
						mazePanel.invalidate();
						Thread.sleep(200);
					}

				}
			}
		}
		if(robot.hasStopped()){
			System.out.println("Robot Stopped");
		}
		
		if(robot.isAtExit()){
			for(Direction direction : Robot.Direction.values()){
				if(robot.canSeeThroughTheExitIntoEternity(direction)){
					switch (direction){
						case LEFT:
							robot.rotate(Turn.LEFT);
							robot.move(1, false);
							break;
						case RIGHT:
							robot.rotate(Turn.RIGHT);
							robot.move(1, false);
							break;
						case BACKWARD:
							robot.rotate(Turn.AROUND);
							robot.move(1, false);
							break;
						case FORWARD:
							robot.move(1, false);
							break;
					}
				}
			}
		}
		return false;
	}
	
	@Override
	public float getEnergyConsumption() {
		return this.startingBattery - robot.getBatteryLevel();
	}

	@Override
	public int getPathLength() {
		return robot.getOdometerReading();
	}
	@Override
	public void setPanel(MazePanel mazePanel){
		this.mazePanel = mazePanel;
	}

}
