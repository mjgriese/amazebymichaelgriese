/**
 * This activity encompasses the winning page. If the user wins the game then this page will display.
 * @author Michael Griese
 * @date November 18, 2019
 */
package edu.wm.cs.cs301.amazebymichaelgriese.gui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import edu.wm.cs.cs301.amazebymichaelgriese.R;

public class WinningActivity extends AppCompatActivity {

    private TextView energyUsed, minPath, pathlength;
    /**
     * Sets the XML layout of the page. Nothing else to do here yet.
     * @param savedInstanceState asdasd
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_winning);

        energyUsed = findViewById(R.id.energyUsed);
        minPath = findViewById(R.id.minPath);
        pathlength = findViewById(R.id.pathLength);

        pathlength.setText(getIntent().getStringExtra("PATH"));

        Float batteryFloat = Float.parseFloat(getIntent().getStringExtra("BATTERY"));
        Float energyFloat = Float.parseFloat(getIntent().getStringExtra("ENERGY"));
        double energyDiff = energyFloat - batteryFloat ;
        energyUsed.setText(String.valueOf(energyDiff));


        minPath.setText(getIntent().getStringExtra("MIN_PATH"));

    }

    /**
     * Returns the user to the main menu if the back button is clicked
     */
    @Override
    public void onBackPressed() {
        Log.v("WinningActivity", "back button pressed");
        Intent titlePage = new Intent(this, AMazeActivity.class);
        startActivity(titlePage);
    }

    private void log(String string) {
        Toast.makeText(getApplicationContext(), string, Toast.LENGTH_SHORT).show();
        Log.v("WinningActivity", string);
    }
}
