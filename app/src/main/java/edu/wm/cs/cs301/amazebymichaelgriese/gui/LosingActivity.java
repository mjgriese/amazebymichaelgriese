/**This activity defines the losing screen that is displayed if the user runs out of energy or if
 * the robot is unable to complete the maze.
 * @author Michael Griese
 * @date November 18, 2019
 */
package edu.wm.cs.cs301.amazebymichaelgriese.gui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import edu.wm.cs.cs301.amazebymichaelgriese.R;

public class LosingActivity extends AppCompatActivity {

    /**
     * Main method, really does nothing except set the content view of the page. Later will
     * need to display values passed from the other states.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_losing);

        TextView energyText = findViewById(R.id.energyUsed);
        TextView failureText = findViewById(R.id.failureText);
        TextView pathLength = findViewById(R.id.pathLength);
        TextView minPath = findViewById(R.id.minPath);

        Float batteryFloat = Float.parseFloat(getIntent().getStringExtra("BATTERY"));
        Float energyFloat = Float.parseFloat(getIntent().getStringExtra("ENERGY"));
        double energyDiff = energyFloat - batteryFloat;
        energyText.setText(String.valueOf(energyDiff));

        pathLength.setText(getIntent().getStringExtra("PATH"));

        switch (getIntent().getStringExtra("REASON")){
            case "POWER_LOSS":
                failureText.setText("Battery Depleted");
                break;
            case "SENSOR_FAILURE":
                failureText.setText("Robot Sensors Failed");
                break;
        }

        minPath.setText(getIntent().getStringExtra("MIN_PATH"));
    }

    /**
     * This method overrides the default back button behavior to return to the main menu.
     */
    @Override
    public void onBackPressed() {
        Log.v("LosingActivity", "back button pressed");
        Intent titlePage = new Intent(this, AMazeActivity.class);
        startActivity(titlePage);
    }
}
