/**This is the generation page that is displayed to the user. From this page a maze is either
 * generated from scratch or loaded from a file.
 * @author Michael Griese
 * @date November 18, 2019
 */
package edu.wm.cs.cs301.amazebymichaelgriese.gui;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.util.Log;

import java.util.Iterator;
import java.util.Set;

import edu.wm.cs.cs301.amazebymichaelgriese.R;
import edu.wm.cs.cs301.amazebymichaelgriese.generation.MazeFactory;
import edu.wm.cs.cs301.amazebymichaelgriese.generation.MazeOrder;
import edu.wm.cs.cs301.amazebymichaelgriese.generation.staticMazeHolder;

public class GeneratingActivity extends AppCompatActivity {

    private ProgressBar generatingBar;
    private int currentProgress = 0;
    private Handler handler = new Handler();
    private MazeOrder order;
    private Thread genThread;
    private MazeFactory factory;


    /**
     *This method associates all the images and items on the screen with java references.
     * The animation for the cow is defined in this method.
     * The thread for the bar updates is also created in this method.
     * @param savedInstanceState again, this is a mandatory param no idea what it does
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generating_layout);


        generatingBar = findViewById(R.id.generationBar);

        generatingBar.setVisibility(View.VISIBLE);
        generatingBar.setProgress(currentProgress);
        generatingBar.setMax(100);

        ImageView loadingCow = findViewById(R.id.loadingCow);
        Animation moveUp = new TranslateAnimation(0,0,0, -700);
        moveUp.setDuration(2000);
        moveUp.setRepeatCount(Animation.INFINITE);
        moveUp.setRepeatMode(Animation.REVERSE);
        moveUp.setFillAfter(true);
        loadingCow.startAnimation(moveUp);


        genThread = new Thread(){
            public void run() {
                generateMaze();
            }
        };

        genThread.start(); //starts the generation thread as defined above
    }


    /**
     * This is a logging method that shows the user a toast of the passed string and logs it
     * using Log.v() the tag for these entries is (GeneratingActivity)
     * @param string string to be toasted and logged
     */
    private void log(String string) {
        Toast.makeText(getApplicationContext(), string, Toast.LENGTH_SHORT).show();
        Log.v("GeneratingActivity", string);
    }

    /**
     * This method overrides the default back button and forces the main menu to appear.
     */
    @Override
    public void onBackPressed() {
        log("back pressed");
        factory.cancel();
        genThread.interrupt();
        Intent titlePage = new Intent(this, AMazeActivity.class);
        startActivity(titlePage);
    }

    /**
     * This returns true if the maze generates correct false if it fucks up
    */
    private void generateMaze() {
        factory = new MazeFactory();
        boolean isPerfect = false;
        switch (getIntent().getStringExtra("ROOMS")) {
            case "No":
                isPerfect = true;
                break;
            case"Yes":
                isPerfect = false;
                break;
        }
        if(getIntent().getStringExtra("GENERATE").equals("false")){
            if(staticMazeHolder.getContainedMaze() == null){
                finish();
                return;
            }
            else{
                if(getIntent().getStringExtra("SOLVER").equals("Manual")) {
                    Intent playScreen = new Intent(getApplicationContext(), PlayManuallyActivity.class);
                    playScreen.putExtra("ENERGY", getIntent().getStringExtra("ENERGY"));
                    finish();
                    startActivity(playScreen);
                    return;
                }
                else{
                    Intent watchScreen = new Intent(getApplicationContext(), PlayAnimationActivity.class);
                    watchScreen.putExtra("SOLVER", getIntent().getStringExtra("SOLVER"));
                    watchScreen.putExtra("ENERGY", getIntent().getStringExtra("ENERGY"));
                    finish();
                    dumpIntent(watchScreen);
                    startActivity(watchScreen);
                    return;
                }
            }
        }
        else {
            order = new MazeOrder(Integer.parseInt(getIntent().getStringExtra("DIFF")), getIntent().getStringExtra("GENERATE"), isPerfect);
        }
        factory.order(order);
        int progress = order.getPrecentComplete();
        while(progress < 100) {
            if (order.getPrecentComplete() > progress) {
                progress = order.getPrecentComplete();
                generatingBar.setProgress(progress);
            }
        }
        //wait for the maze to generate
        if(factory.isStillGenerating()) {
            factory.waitTillDelivered();
        }
        if(staticMazeHolder.getContainedMaze() == null) {
            finish();
        }
        else {
            if(getIntent().getStringExtra("SOLVER").equals("Manual")) {
                Intent playScreen = new Intent(getApplicationContext(), PlayManuallyActivity.class);
                playScreen.putExtra("ENERGY", getIntent().getStringExtra("ENERGY"));
                startActivity(playScreen);
            }
            else{
                Intent watchScreen = new Intent(getApplicationContext(), PlayAnimationActivity.class);
                watchScreen.putExtra("SOLVER", getIntent().getStringExtra("SOLVER"));
                watchScreen.putExtra("ENERGY", getIntent().getStringExtra("ENERGY"));
                startActivity(watchScreen);
            }
        }
    }

    /**
     * Toasts the skill level and  build type stored in the order object
     */
    private void toastOrder(){
        switch (order.getBuilder()) {
            case DFS:
                log("Order has DFS/Default");
                break;

            case Prim:
                log("Order has Prim");
                break;

            case Eller:
                log("Order has Eller");
                break;
        }
        log(String.valueOf(order.getSkillLevel()));
    }

    public static void dumpIntent(Intent i){

        Bundle bundle = i.getExtras();
        if (bundle != null) {
            Set<String> keys = bundle.keySet();
            Iterator<String> it = keys.iterator();
            Log.e("ASS","Dumping Intent start");
            while (it.hasNext()) {
                String key = it.next();
                Log.e("ASS","[" + key + "=" + bundle.get(key)+"]");
            }
            Log.e("ASS","Dumping Intent end");
        }
    }
}
