package edu.wm.cs.cs301.amazebymichaelgriese.gui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import edu.wm.cs.cs301.amazebymichaelgriese.R;
import edu.wm.cs.cs301.amazebymichaelgriese.generation.CardinalDirection;
import edu.wm.cs.cs301.amazebymichaelgriese.generation.Floorplan;
import edu.wm.cs.cs301.amazebymichaelgriese.generation.Maze;
import edu.wm.cs.cs301.amazebymichaelgriese.generation.staticMazeHolder;

public class PlayManuallyActivity extends AppCompatActivity {

    private boolean triggerEnd = true;

    private Button mazeButton;
    private boolean mazeStatus = false;

    private Button wallButton;
    private boolean wallStatus = false;

    private Button solutionButton;
    private boolean solutionStatus = false;

    private TextView energyText;
    private float energyRemaining = 3000.0f;
    private ProgressBar energyBar;


    private float mapScale = 1.0f;
    private Button mapIn;
    private Button mapOut;
    private TextView scaleText;

    private MazePanel mazePanel;

    private Maze mazeConfig = staticMazeHolder.getContainedMaze();
    private Floorplan seenCells;
    private FirstPersonView firstPerson = null;
    private Map map = null;

    private int px; //these will hold current X and Y values respectively
    private int py;

    private int dx;
    private int dy; //these hold the direction to move next

    private int angle; //this holds the angle the camera should face
    private int minPath;

    private int walkStep;

    private Robot robot = new BasicRobot();


    /**
     * Main, as usual it add references to all the parts of the XML file. It also handles their text
     * and onClick handlers.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_manually);

        mazePanel = findViewById(R.id.mazePanel);

        robot.setBatteryLevel(Float.parseFloat(getIntent().getStringExtra("ENERGY")));

        mapIn = findViewById(R.id.mapIn);
        mapOut = findViewById(R.id.mapOut);
        scaleText = findViewById(R.id.mapScale);
        mapIn.setVisibility(View.INVISIBLE);
        mapOut.setVisibility(View.INVISIBLE);
        scaleText.setVisibility(View.INVISIBLE);
        scaleText.setText(String.valueOf(mapScale));

        mapIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                log("Zooming In");
                mapScale += .25;
                scaleText.setText(String.valueOf(mapScale));
                mazePanel.incrementMapScale();
                mazePanel.invalidate();
            }
        });

        mapOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Float scale = Float.parseFloat(String.valueOf(mapScale));
                if(Float.compare(scale, 0.0f) > 0.0f); {
                    log("Zooming Out");
                    mapScale -= .25;
                    scaleText.setText(String.valueOf(mapScale));
                    mazePanel.decrementMapScale();
                    mazePanel.invalidate();
                }
            }
        });

        energyText = findViewById(R.id.energyRemaining);
        energyBar = findViewById(R.id.energyBar);
        int maxEnergy = Integer.parseInt(getIntent().getStringExtra("ENERGY"));
        energyBar.setMax(maxEnergy);
        energyBar.setProgress(maxEnergy);
        energyText.setText(getIntent().getStringExtra("ENERGY"));
        //associates the buttons to the class
        wallButton = findViewById(R.id.wallButton);
        mazeButton = findViewById(R.id.mazeButton);
        solutionButton = findViewById(R.id.solutionButton);
        //sets the text for each button
        mazeButton.setText("OFF");
        wallButton.setText("OFF");
        solutionButton.setText("OFF");


        wallButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wallStatus = !wallStatus;
                mazePanel.toggleWalls();
                mazePanel.invalidate();
                log("Toggling Walls");
                if(wallStatus) {
                    wallButton.setText("ON");
                }
                else {
                    wallButton.setText("OFF");
                }
            }
        });

        solutionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                solutionStatus = !solutionStatus;
                mazePanel.toggleSolution();
                mazePanel.invalidate();
                log("Toggling Solution");
                if(solutionStatus) {
                    solutionButton.setText("ON");
                }
                else {
                    solutionButton.setText("OFF");
                }
            }
        });

        mazeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mazeStatus = !mazeStatus;
                mazePanel.toggleMaze();
                mazePanel.invalidate();
                log("Toggling Maze");
                if(mazeStatus) {
                    mazeButton.setText("ON");
                    mapIn.setVisibility(View.VISIBLE);
                    mapOut.setVisibility(View.VISIBLE);
                    scaleText.setVisibility(View.VISIBLE);
                }
                else {
                    mazeButton.setText("OFF");
                    mapIn.setVisibility(View.INVISIBLE);
                    mapOut.setVisibility(View.INVISIBLE);
                    scaleText.setVisibility(View.INVISIBLE);
                }
            }
        });


        ImageView upArrow = findViewById(R.id.upArrow);
        upArrow.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
               longClickUp(v);
                return true;
            }
        });

        setPositionDirectionViewingDirection();
        seenCells = new Floorplan(mazeConfig.getWidth()+1,mazeConfig.getHeight()+1) ;
    }

    /**
     * This method decrements the remaining energy and changes the progress of the energy bar.
     * In the future, this number should be gotten directly from the robot rather than have a
     * duplicate stored here.
     */
    public void updateEnergy() {
        energyRemaining = robot.getBatteryLevel();
        energyBar.setProgress((int)energyRemaining);
        energyText.setText(String.valueOf(energyRemaining));
    }

    /**
     * user clicks the up arrow to go forward.
     * @param v current view
     */
    public void clickUp(View v) {
        updateEnergy();
        log("click up");
        keyDown(Constants.UserInput.Up);
        updateEnergy();
        mazePanel.invalidate();
    }


    public void longClickUp(View v){
        updateEnergy();
        log("long click up");
        keyDown(Constants.UserInput.Jump);
        updateEnergy();
        mazePanel.invalidate();
    }
    /**
     * user clicks the right arrow to turn right.
     * @param v current view
     */
    public void clickRight(View v){
        updateEnergy();
        log("click right");
        keyDown(Constants.UserInput.Right);
        updateEnergy();
        mazePanel.invalidate();
    }
    /**
     * user clicks the left arrow to turn left.
     * @param v current view
     */
    public void clickLeft(View v){
        updateEnergy();
        log("click Left");
        keyDown(Constants.UserInput.Left);
        updateEnergy();
        mazePanel.invalidate();
    }

    /**
     * Logs the string and shows a toast with the string text.
     * @param string string to toast and log.
     */
    private void log(String string) {
        //Toast.makeText(getApplicationContext(), string, Toast.LENGTH_SHORT).show();
        Log.v("PlayManuallyActivity", string);
    }

    /**
     * Overrides the default back behavior to return to the main menu screen
     */
    @Override
    public void onBackPressed() {
        log("back pressed");
        Intent titlePage = new Intent(this, AMazeActivity.class);
        finish();
        startActivity(titlePage);
    }

    /**
     * Method incorporates all reactions to keyboard input in original code,
     * The simple key listener calls this method to communicate input.
     * Method requires {MazePanel) start} to be
     * called before.
     * @param key is the action that should be executed
     * @return false if not started yet otherwise true
     */
    public boolean keyDown(Constants.UserInput key) {
        // react to input for directions and interrupt signal (ESCAPE key)
        // react to input for displaying a map of the current path or of the overall maze (on/off toggle switch)
        // react to input to display solution (on/off toggle switch)
        // react to input to increase/reduce map scale
        switch (key) {
            case Start: // misplaced, do nothing
                break;
            case Up: // move forward
                if(robot.getBatteryLevel() > robot.getEnergyForStepForward()){
                    walk(1);
                    robot.setBatteryLevel(robot.getBatteryLevel() - robot.getEnergyForStepForward());
                    robot.incrementOdometer();
                    // check termination, did we leave the maze?
                    if (isOutside(px,py)) {
                        Intent winScreen = new Intent(getApplicationContext(), WinningActivity.class);
                        winScreen.putExtra("ENERGY", getIntent().getStringExtra("ENERGY"));
                        winScreen.putExtra("BATTERY", String.valueOf(robot.getBatteryLevel()));
                        winScreen.putExtra("PATH", String.valueOf(robot.getOdometerReading()));
                        winScreen.putExtra("MIN_PATH", String.valueOf(minPath));
                        log(String.valueOf(robot.getBatteryLevel()));
                        log(String.valueOf(robot.getOdometerReading()));
                        log(String.valueOf(minPath));
                        startActivity(winScreen);
                    }
                }
                else{
                    robot.setHasStopped(true);
                    robot.setBatteryLevel(0.0f);
                }
                break;
            case Left: // turn left
                if(robot.getBatteryLevel() >= (robot.getEnergyForFullRotation()/4)){
                    rotate(1);
                    robot.setBatteryLevel(robot.getBatteryLevel() - (robot.getEnergyForFullRotation()/4));
                    robot.incrementOdometer();
                }
                else{
                    robot.setHasStopped(true);
                    robot.setBatteryLevel(0.0f);
                }
                break;
            case Right: // turn right
                if(robot.getBatteryLevel() >= (robot.getEnergyForFullRotation()/4)){
                    rotate(-1);
                    robot.setBatteryLevel(robot.getBatteryLevel() - (robot.getEnergyForFullRotation()/4));
                    robot.incrementOdometer();
                }
                else{
                    robot.setHasStopped(true);
                    robot.setBatteryLevel(0.0f);
                }
                break;
            case Down: // move backward
                if(robot.getBatteryLevel() > robot.getEnergyForStepForward()){
                    walk(-1);
                    robot.setBatteryLevel(robot.getBatteryLevel() - robot.getEnergyForStepForward());
                    robot.incrementOdometer();
                    // check termination, did we leave the maze?
                    if (isOutside(px,py)) {
                        Intent winScreen = new Intent(getApplicationContext(), WinningActivity.class);
                        winScreen.putExtra("ENERGY", getIntent().getStringExtra("ENERGY"));
                        String batteryString = String.valueOf(robot.getBatteryLevel());
                        winScreen.putExtra("BATTERY", batteryString);
                        winScreen.putExtra("PATH_LENGTH", String.valueOf(robot.getOdometerReading()));
                        winScreen.putExtra("MIN_PATH", String.valueOf(minPath));
                        startActivity(winScreen);
                    }
                }
                else{
                    robot.setHasStopped(true);
                    robot.setBatteryLevel(0.0f);
                }
                break;
            case ReturnToTitle: // escape to title screen
                finish();
                break;
            case Jump: // make a step forward even through a wall
                // go to position if within maze
                if(robot.getBatteryLevel() > (robot.getEnergyForStepForward()*10)){
                    if (mazeConfig.isValidPosition(px + dx, py + dy)) {
                        setCurrentPosition(px + dx, py + dy) ;
                        robot.incrementOdometer();
                        robot.setBatteryLevel(robot.getBatteryLevel() - (10*robot.getEnergyForStepForward()));
                        draw() ;
                    }
                }
                else{
                    robot.setHasStopped(true);
                    robot.setBatteryLevel(0.0f);
                }
                break;
        } // end of internal switch statement for playing state
        updatePanel(); //THIS UPDATES THE XY CORDS AND ANGLE IN MAZE PANEL WITHOUT IT YOU CANNOT MOVE PAST THE START POSITION
        //this should swap to the next state if the robot has no power
        //System.out.println(getRobot.getBatteryLevel());
        int batteryInt = (int)robot.getBatteryLevel();
        if((batteryInt <= 0)&&triggerEnd){
            triggerEnd = false;
            robot.toggleDrive();
            Intent lossScreen = new Intent(getApplicationContext(), LosingActivity.class);
            lossScreen.putExtra("ENERGY", getIntent().getStringExtra("ENERGY"));
            lossScreen.putExtra("BATTERY", String.valueOf(robot.getBatteryLevel()));
            lossScreen.putExtra("PATH", String.valueOf(robot.getOdometerReading()));
            lossScreen.putExtra("REASON", "POWER_LOSS");
            lossScreen.putExtra("MIN_PATH", String.valueOf(minPath));

            startActivity(lossScreen);
        }
        return true;
    }


    private void draw(){mazePanel.invalidate();}

    /**
     * Moves in the given direction with 4 intermediate steps,
     * updates the screen and the internal position
     * @param dir, only possible values are 1 (forward) and -1 (backward)
     */
    synchronized private void walk(int dir) {
        // check if there is a wall in the way
        if (!checkMove(dir))
            return;
        // walkStep is a parameter of FirstPersonDrawer.draw()
        // it is used there for scaling steps
        // so walkStep is implicitly used in slowedDownRedraw
        // which triggers the draw operation in
        // FirstPersonDrawer and MapDrawer
        for (int step = 0; step != 4; step++) {
            walkStep += dir;
        }
        setCurrentPosition(px + dir*dx, py + dir*dy) ;
        walkStep = 0; // reset counter for next time
    }


    /**
     * Helper method for walk()
     * @param dir distance to walks
     * @return true if there is no wall in this direction
     */
    protected boolean checkMove(int dir) {
        getCurrentPosition();
        CardinalDirection cd = null;
        switch (dir) {
            case 1: // forward
                cd = getCurrentDirection();
                break;
            case -1: // backward
                cd = getCurrentDirection().oppositeDirection();
                break;
            default:
                throw new RuntimeException("Unexpected direction value: " + dir);
        }
        return !mazeConfig.hasWall(px, py, cd);
    }

    protected int[] getCurrentPosition() {
        int[] result = new int[2];
        result[0] = px;
        result[1] = py;
        return result;
    }
    protected void setCurrentPosition(int x, int y) {
        px = x ;
        py = y ;
    }
    private void setCurrentDirection(int x, int y) {
        dx = x ;
        dy = y ;
    }
    /**
     * Internal method to set the current position, the direction
     * and the viewing direction to values consistent with the
     * given maze.
     */
    private void setPositionDirectionViewingDirection() {
        // obtain starting position
        int[] start = mazeConfig.getStartingPosition() ;
        setCurrentPosition(start[0],start[1]) ;
        minPath = mazeConfig.getDistanceToExit(start[0], start[1]);
        // set current view direction and angle
        angle = 0; // angle matches with east direction,
        // hidden consistency constraint!
        setDirectionToMatchCurrentAngle();
        // initial direction is east, check this for sanity:
        assert(dx == 1);
        assert(dy == 0);
        updatePanel();
    }

    private void setDirectionToMatchCurrentAngle() {
        setCurrentDirection((int) Math.cos(radify(angle)), (int) Math.sin(radify(angle))) ;
    }

    final double radify(int x) {
        return x*Math.PI/180;
    }
    protected CardinalDirection getCurrentDirection() {
        return CardinalDirection.getDirection(dx, dy);
    }

    /**
     * Performs a rotation with 4 intermediate views,
     * updates the screen and the internal direction
     * @param dir for current direction, values are either 1 or -1
     */
    synchronized private void rotate(int dir) {
        final int originalAngle = angle;
        final int steps = 4;

        for (int i = 0; i != steps; i++) {
            // add 1/4 of 90 degrees per step
            // if dir is -1 then subtract instead of addition
            angle = originalAngle + dir*(90*(i+1))/steps;
            angle = (angle+1800) % 360;
            // draw method is called and uses angle field for direction
            // information.
        }
        // update maze direction only after intermediate steps are done
        // because choice of direction values are more limited.
        setDirectionToMatchCurrentAngle();
        //logPosition(); // debugging
    }

    private boolean isOutside(int x, int y) {
        return !mazeConfig.isValidPosition(x, y) ;
    }

    private void updatePanel(){
        mazePanel.setPx(px);
        mazePanel.setPy(py);
        mazePanel.setAngle(angle);
        mazePanel.invalidate();
    }
}
