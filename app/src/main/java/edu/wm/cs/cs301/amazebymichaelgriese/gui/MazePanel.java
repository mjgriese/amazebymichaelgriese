package edu.wm.cs.cs301.amazebymichaelgriese.gui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;


import edu.wm.cs.cs301.amazebymichaelgriese.generation.CardinalDirection;
import edu.wm.cs.cs301.amazebymichaelgriese.generation.Floorplan;
import edu.wm.cs.cs301.amazebymichaelgriese.generation.Maze;
import edu.wm.cs.cs301.amazebymichaelgriese.generation.staticMazeHolder;


/**
 * Custom View class for android that allows the drawing of a maze as well as user interaction with
 * that said maze.
 * @author Michael Griese
 *
 */
public class MazePanel extends View {

	private Canvas mazeCanvas;
	private Paint mazePaint;
	private Bitmap bitmap;

	private int mapScale;
	private int mapUnit;
	private int stepSize;

	private int px, py; //these hold current x/y cords
	private int angle;	//holds direction angle to point camera

	private int walkStep = 1;

	private FirstPersonView firstPerson;
	private Map map;


	private boolean isMapVisible = false;
	private boolean isSolutionVisible = false;
	private boolean isSeenWallsVisible = false;

	public MazePanel(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	private void init() {
		mazePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		mazePaint.setStrokeWidth(7);

		Maze mazeConfig = staticMazeHolder.getContainedMaze();
		Floorplan seenCells = new Floorplan(mazeConfig.getWidth()+1,mazeConfig.getHeight()+1);

		firstPerson = new FirstPersonView(Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT, Constants.MAP_UNIT, Constants.STEP_SIZE, seenCells, mazeConfig.getRootnode());
		map = new Map(seenCells, 30, mazeConfig) ;

	}

	@Override
	protected void onDraw(Canvas canvas) {
		mazeCanvas = canvas;
		super.onDraw(canvas);
		update();
		//paintTest();

	}

	public void update() {
		if(mazeCanvas == null){
			return;
		}
		firstPerson.draw(this, px, py, walkStep, angle);
		if(isMapVisible){
			map.draw(this, px, py, angle, walkStep, isSeenWallsVisible, isSolutionVisible);
		}
	}

	public void setColor(customColor color) {
		mazePaint.setARGB(255, color.getR(), color.getG(), color.getB());
	}

	public void drawCircle(float centerX, float centerY, float radius) {
		mazeCanvas.drawCircle(centerX, centerY, radius, mazePaint);
	}

	public void drawRect(float left, float top, float right, float bottom){
		mazeCanvas.drawRect(left, top, right, bottom, mazePaint);
	}

	/**
	 * Private method to ensure the color setting and shape drawing is functional
	 */
	private void paintTest(){
		setColor(customColor.green);
		drawCircle(0,0, 100);

		setColor(customColor.yellow);
		drawRect(25, 25, 1225, 1225);

		setColor(customColor.red);
		drawCircle(500, 500, 50);

		setColor(customColor.black);
		fillPolygon(new int[] {1, 25, 55, 100, 500}, new int[] {3, 50, 75, 200, 750}, 5);

		setColor(new customColor(12, 45, 89));
		fillRect(450, 450, -50, -50);

		setColor(new customColor(161, 245, 66));
		fillOval(900, 900, 140, 200);
	}

	public void fillPolygon(int[] xps, int[] xpy, int numberOfPoints){
		Path polygon = new Path();
		polygon.moveTo(xps[0], xpy[0]);
		for(int i = 1; i < numberOfPoints; i++){
			polygon.lineTo(xps[i], xpy[i]);
		}
		polygon.close();
		mazeCanvas.drawPath(polygon, mazePaint);
	}

	public void fillRect(float x, float y, float width, float height) {
		mazeCanvas.drawRect(x, y, x + width, y + height, mazePaint);
	}

	/**Draws the black background of the maze
	 */
	public void drawBackground() {
		// black rectangle in upper half of screen
		setColor(customColor.black);
		fillRect(0, 0, Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT/2);
		// grey rectangle in lower half of screen
		setColor(customColor.darkGray);
		fillRect(0, Constants.VIEW_HEIGHT/2, Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT/2);
	}


	/**This method draws the map on the screen if drawMap is true
	 */
	public void drawMap(int px, int py, int walkStep, int viewDX, int viewDY, boolean showMaze, boolean showSolution, Maze maze, int mapScale, Floorplan seenWalls, int mapUnit, int stepSize) {
		// dimensions of the maze in terms of cell ids
		final int mazeWidth = maze.getWidth() ;
		final int mazeHeight = maze.getHeight() ;
		this.mapScale = mapScale;
		this.mapUnit = mapUnit;
		this.stepSize = stepSize;


		setColor(customColor.white);

		// note: 1/2 of width and height is the center of the screen
		// the whole map is centered at the current position
		final int offsetX = getOffset(px, walkStep, viewDX, Constants.VIEW_WIDTH);
		final int offsetY = getOffset(py, walkStep, viewDY, Constants.VIEW_HEIGHT);

		// We need to calculate bounds for cell indices to consider
		// for drawing. Since not the whole maze may be visible
		// for the given screen size and the current position (px,py)
		// is fixed to the center of the drawing area, we need
		// to find the min and max indices for cells to consider.
		// compute minimum for x,y
		final int minX = getMinimum(offsetX);
		final int minY = getMinimum(offsetY);
		// compute maximum for x,y
		final int maxX = getMaximum(offsetX, Constants.VIEW_WIDTH, mazeWidth);
		final int maxY = getMaximum(offsetY, Constants.VIEW_HEIGHT, mazeHeight);

		// iterate over integer grid between min and max of x,y indices for cells
		for (int y = minY; y <= maxY; y++)
			for (int x = minX; x <= maxX; x++) {
				// starting point of line
				int startX = mapToCoordinateX(x, offsetX);
				int startY = mapToCoordinateY(y, offsetY);

				// draw horizontal line
				boolean theCondition = (x < mazeWidth) && ((y < mazeHeight) ?
						maze.hasWall(x, y, CardinalDirection.North) :
						maze.hasWall(x, y - 1, CardinalDirection.South));

				setColor(seenWalls.hasWall(x,y, CardinalDirection.North) ? customColor.white : customColor.gray);
				if ((seenWalls.hasWall(x,y, CardinalDirection.North) || showMaze) && theCondition)
					drawLine(startX, startY, startX + mapScale, startY); // y coordinate same

				// draw vertical line
				theCondition = (y < mazeHeight) && ((x < mazeWidth) ?
						maze.hasWall(x, y, CardinalDirection.West) :
						maze.hasWall((x - 1), y, CardinalDirection.East));

				setColor(seenWalls.hasWall(x,y, CardinalDirection.West) ? customColor.white : customColor.gray);
				if ((seenWalls.hasWall(x,y, CardinalDirection.West) || showMaze) && theCondition)
					drawLine(startX, startY, startX, startY - mapScale); // x coordinate same
			}

		if (showSolution) {
			drawSolution(offsetX, offsetY, px, py, maze) ;
		}
	}


	/**
	 * Draws a yellow line to show the solution on the overall map.
	 * Method is only called if in state playing and map_mode
	 * and showSolution are true.
	 * Since the current position is fixed at the center of the screen,
	 * all lines on the map are drawn with some offset
	 * @param offsetX is the offset for x coordinates
	 * @param offsetY is the offset for y coordinates
	 * @param px is the current position, an index x for a cell
	 * @param py is the current position, an index y for a cell
	 * @param maze
	 */
	private void drawSolution(int offsetX, int offsetY, int px, int py, Maze maze) {
		if (!maze.isValidPosition(px, py)) {
			dbg(" Parameter error: position out of bounds: (" + px + "," +
					py + ") for maze of size " + maze.getWidth() + "," +
					maze.getHeight()) ;
			return ;
		}
		// current position on the solution path (sx,sy)
		int sx = px;
		int sy = py;
		int distance = maze.getDistanceToExit(sx, sy);

		setColor(customColor.yellow);

		// while we are more than 1 step away from the final position
		while (distance > 1) {
			// find neighbor closer to exit (with no wallboard in between)
			int[] neighbor = maze.getNeighborCloserToExit(sx, sy) ;
			if (null == neighbor)
				return ; // error

			int nx1 = mapToCoordinateX(sx,offsetX) + mapScale/2;
			int ny1 = mapToCoordinateY(sy,offsetY) - mapScale/2;
			// neighbor position coordinates
			int nx2 = mapToCoordinateX(neighbor[0],offsetX) + mapScale/2;
			int ny2 = mapToCoordinateY(neighbor[1],offsetY) - mapScale/2;
			drawLine(nx1, ny1, nx2, ny2);

			// update loop variables for current position (sx,sy)
			// and distance d for next iteration
			sx = neighbor[0];
			sy = neighbor[1];
			distance = maze.getDistanceToExit(sx, sy) ;
		}

	}

	private int getMaximum(int offset, int viewLength, int mazeLength) {
		int result = (viewLength-offset)/mapScale+1;
		if (result >= mazeLength)
			result = mazeLength;
		return result;
	}

	/**
	 * Obtains the minimum for a given offset
	 * @param offset either in x or y direction
	 * @return minimum that is greater or equal 0
	 */
	private int getMinimum(final int offset) {
		final int result = -offset/mapScale;
		return (result < 0) ? 0 : result;
	}

	/**
	 * Calculates the offset in either x or y direction
	 * @param coordinate is either x or y coordinate of current position
	 * @param walkStep distance you walk
	 * @param viewDirection is either viewDX or viewDY
	 * @param viewLength is either viewWidth or viewHeight
	 * @return the offset
	 */
	private int getOffset(int coordinate, int walkStep, int viewDirection, int viewLength) {
		final int tmp = coordinate*mapUnit + mapUnit/2 + mapToOffset((stepSize*walkStep),viewDirection);
		return -tmp*mapScale/mapUnit + viewLength/2;
	}

	/**
	 * Maps a given length and direction into an offset for drawing coordinates.
	 * @param length length
	 * @param direction direction
	 * @return offset
	 */
	private int mapToOffset(int length, int direction) {
		// Signed bit shift to the right performs a division by 2^16
		// preserves the sign
		// discards the remainder as the result is int
		return (length * direction) >> 16;
	}

	/**
	 * Maps the y index for some cell (x,y) to a y coordinate
	 * for drawing.
	 * @param cellY, {@code 0 <= cellY < height}
	 * @param offsetY y offset
	 * @return y coordinate for drawing
	 */
	private int mapToCoordinateY(int cellY, int offsetY) {
		return Constants.VIEW_HEIGHT -1-(cellY*mapScale + offsetY);
	}

	/**
	 * Maps the x index for some cell (x,y) to an x coordinate
	 * for drawing.
	 * @param cellX is the index of some cell, {@code 0 <= cellX < width}
	 * @param offsetX x offset
	 * @return x coordinate for drawing
	 */
	private int mapToCoordinateX(int cellX, int offsetX) {
		return cellX*mapScale + offsetX;
	}

	/**
	 * Draws a red circle at the center of the screen and
	 * an arrow for the current direction.
	 * It always reside on the center of the screen.
	 * The map drawing moves if the user changes location.
	 * The size of the overall visualization is limited by
	 * the size of a single cell to avoid that the circle
	 * or arrow visually collide with an adjacent wallboard on the
	 * map visualization.
	 */
	public void drawCurrentLocation(int viewDX, int viewDY) {
		setColor(customColor.red);
		// draw oval of appropriate size at the center of the screen
		int centerX = Constants.VIEW_WIDTH/2; // center x
		int centerY = Constants.VIEW_HEIGHT/2; // center y
		int diameter = mapScale/2; // circle size
		// we need the top left corner of a bounding box the circle is in
		// and its width and height to draw the circle
		// top left corner is (centerX-radius, centerY-radius)
		// width and height is simply the diameter
		fillOval(centerX-diameter/2, centerY-diameter/2, diameter, diameter);
		// draw a red arrow with the oval to show current direction
		drawArrow(viewDX, viewDY, centerX, centerY);
	}

	/**
	 * Draws an arrow either in horizontal or vertical direction.
	 *
	 * @param viewDX is the current viewing direction, x coordinate
	 * @param viewDY is the current viewing direction, y coordinate
	 * @param startX is the x coordinate of the starting point
	 * @param startY is the y coordinate of the starting point
	 */
	private void drawArrow(int viewDX, int viewDY, int startX, int startY) {
		// calculate length and coordinates for main line
		final int arrowLength = mapScale*7/16; // arrow length, about 1/2 map_scale
		final int tipX = startX + mapToOffset(arrowLength, viewDX);
		final int tipY = startY - mapToOffset(arrowLength, viewDY);
		// draw main line, goes from starting (x,y) to end (tipX,tipY)
		drawLine(startX, startY, tipX, tipY);
		// calculate length and positions for 2 lines pointing towards (tipX,tipY)
		// find intermediate point (tmpX,tmpY) on main line
		final int length = mapScale/4;
		final int tmpX = startX + mapToOffset(length, viewDX);
		final int tmpY = startY - mapToOffset(length, viewDY);
		// find offsets at intermediate point for 2 points orthogonal to main line
		// negative sign used for opposite direction
		// note the flip between x and y for view_dx and view_dy
		/*
		final int offsetX = -(length * view_dy) >> 16;
		final int offsetY = -(length * view_dx) >> 16;
		*/
		final int offsetX = mapToOffset(length, -viewDY);
		final int offsetY = mapToOffset(length, -viewDX);
		// draw two lines, starting at tip of arrow
		drawLine(tipX, tipY, tmpX + offsetX, tmpY + offsetY);
		drawLine(tipX, tipY, tmpX - offsetX, tmpY - offsetY);

	}

	/**
	 * Debug output
	 * @param str is the string that will be printed
	 */
	private void dbg(String str) {
		System.out.println("MapDrawer:"+ str);
	}

	public void drawLine(int startX, int startY, int stopX, int stopY) {
		mazeCanvas.drawLine(startX, startY, stopX, stopY, mazePaint);

	}

	public void fillOval(int x, int y, int width, int height) {
		RectF ovalBounds = new RectF(x, y, x + width, y + height);
		mazeCanvas.drawOval(ovalBounds, mazePaint);
	}

	/**
	 * This is a logging method to show the user a toast and ass an entry in Log.v()
	 *
	 * @param string string to be logged and shown to the user via toast
	 */
	private void log(String string) {
		//Toast.makeText(getApplicationContext(), string, Toast.LENGTH_SHORT).show();
		Log.v("PlayAnimationActivity", string);
	}

	protected void toggleMaze(){
		isMapVisible = !isMapVisible;
	}

	protected void toggleSolution(){
		isSolutionVisible = !isSolutionVisible;
	}

	protected void toggleWalls(){
		isSeenWallsVisible = !isSeenWallsVisible;
	}

	protected void setPx(int px){
		this.px = px;
	}
	protected void setPy(int px){
		this.py = px;
	}

	protected void setAngle(int angle){
		this.angle = angle;
	}

	protected void incrementMapScale(){
		map.incrementMapScale();
	}

	protected void decrementMapScale(){
		map.decrementMapScale();
	}

}


