package edu.wm.cs.cs301.amazebymichaelgriese.generation;

public class MazeOrder implements Order {

		private int skillLevel;
		private int precentComplete = 0;
		private boolean isPerfect;
		private Builder buildType; //holds general information about the order 
		private Maze generatedMaze;
		private Boolean isComplete = false;
		
		public MazeOrder(int skillLevel, String builder, boolean isPerfect){
			this.skillLevel = skillLevel;
			this.isPerfect = isPerfect;
			switch (builder) {
				case "Eller":
					this.buildType = Builder.Eller;
					break;
				case "Prim":
					this.buildType = Builder.Prim;
					break;
				default:
					this.buildType = Builder.DFS;
			}
		}
		@Override
		public boolean isPerfect() {
			return isPerfect;
		}

		@Override
		public int getSkillLevel(){
			return this.skillLevel;
		}

		@Override
		public Builder getBuilder(){
			return this.buildType;
		}

		@Override
		public void updateProgress(int precent){
			this.precentComplete = precent;
		}

		@Override
		public void deliver(Maze generatedMaze){
			this.generatedMaze = generatedMaze;
			System.out.println("Order has been fulfilled") ;
		}
	
		public Maze getMaze(){
			return this.generatedMaze;
		}

		public int getPrecentComplete(){
			return  precentComplete;
		}
	}